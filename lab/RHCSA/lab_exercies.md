# Lab Exercises

1. Reset *forgotten* `root` password.

---

2. Manage Users and Groups:
    1. Create groups:
        * `operations`
        * `accounts`
        * `developers`: `GID=1200`
    2. Create user `tom` and allow *superuser* privileges.
    3. Create user `jacob`:
        1. One off password policy
            * Expires: 30 days
            * Expiration warning: 7 days
        2. When creating user, set UID to `1234`.
        3. Assign to: `operations` group

    4. Create users `susan`, `jessica` and `steve`:
        1. Password Policy:
            * Maximum lifetime: 120 days
            * Expiration warning: 3 days
            * Minimum Length: 6 characters
        2. Add `testfile` to be created for all new users.
        3. Starting from UID/GID: 1500
        4. Assign `susan` to `accounts` group.
        5. Assign `jessica` and `steve` to `developers` group.

---

3. On the second NIC, create the following:
    1. Assign a network address dynamically.
        * Ensure route is added.
        * Reboot server.
    2. Networking profile for DHCP
        * Test to make sure an address is assigned.
    3. Static with both IPv4 and IPv6 addresses.
        * This address should be the default connection for this interface.

---

4. Setup SSH Access:
    * Key based authentication
    * Disable root login

---

5. Managing Software:
    1. Create a local *yum* repository for both the BaseOS and AppStream.
    2. Use the local repo - Part One:
        1. Search the repo for a package which contains **sepolicy** program file; do not install.
        2. Download the **httpd** package from the repository (do not install), and query to see if there are any scripts in there.
    3. Using AppStreams:
        1. Find which AppStreams are available for the Perl module.
        2. Install the latest minimal version of Perl.
        3. Downgrade the version of Perl.
        4. Upgrade the version of Perl; ensure dependents are also updated.

---

6. Using the *`MBR` partitioning scheme*, create three partitions:
    * Format one using *XFS* and mount persistently using the UUID.
    * Format one using *VFAT* and mount persistently using a label.
    * Use the remaining for a *SWAP* partition.
    * Verify using tools and check by rebooting server.

---

7. Using the *`GPT` partitioning scheme*, create three partitions:
    * Format one using *XFS* and mount persistently using the UUID.
    * Format one using *EXT4* and mount persistently using a label.
    * Use the remaining for a *SWAP* partition.
    * Verify using tools and check by rebooting server.

---

8. Create a 4GiB volume group, using a physical extent size of 2MiB. In this
volume group, create a 1-GiB logical volume with the name myfiles and
mount it persistently on /myfiles.
    * Extend the logical volume to 3GiB.
    * Shrink the logical volume to 2GiB.

---

9. Setup an NFS share:
    1. On one server, setup an NFS share.
    2. On the second server, mount the share.
    3. On the second server, set up the share with `automount`.

---

10. Setup a CIFS share.
    * On one server, setup the share.
    * On another server, mount the share.

---

11. Setup Apache websites:
    1. Default port and location.
    2. Non-standard Location (`/srv/www/html/`), Non-Standard Port: 101

---

12. Setup an NTP using `chrony`.
    1. Set one server up to act as a time source.
    2. Set another server up to pull it's time from the server.

---

13. Configure Logging
    1. Ensure `journald` is persitently logging.
    2. Create an entry in `rsyslog` which writes all messages with a severity of error or higher in **/var/log/error**
    3. Ensure **/var/log/error** is rotated on a daily basis, with the past 7 days being kept and compressed.

---

14. Stratis
    1. Create a stratis pool named *thepool*:
        1. Start with one block device.
        2. Then add an additional block device.
    2. Create two filesystems.
        1. Mount one using `systemd mounts`.
        2. Mount one using `fstab`.
    3. Snapshots
        1. Create a snapshot of one of the filesystems.
        2. Mount snapshot
    4. Remove stratis pool.

---

16. Managing Permissions
    1. Ensure that others are denied default permission to any file the user `tom` creates.
    2. Create directories for the `accounts`, `developers` and `operations` groups under */data/*.
        1. Members of the group have full read/write access to their directories, others have no permissions at all.
        2. Files created in this directory are writeable for all group members.
        3. Users can only delete files they have created themselves.
        4. `operations` have read access to everything in the `developers` directory.
        5. `tom` is the *CEO* and should be able to delete everything in the */data/* directory.

---

17. Managing Processes
    1. Start `dd` processes:
        1. One with a nice value of `-5`, and send it to the background.
        2. Start three more `dd` processes in the background.
        3. List the background jobs.
    2. Using `top`:
        1. observe the `dd` processes
        2. Renice one of the `dd` processes with a value of -15 and observe.
        3. Kill the `top` process with a `sigkill`.
        4. Terminate the `dd` processes.
    3. Using `tuned`
        1. Get the current `tuned` profile.
        2. List the available `tuned` profiles and switch to one.
        3. Revert the `tuned` profile.

---

18. Working With `systemd`
    1. Install `vsftpd` and `httpd` and make sure `httpd` is started automatically when the system is booted.
    2. Set the default `systemctl` editor to be`vim`.
    3. Edit the `httpd` services so that it automatically starts the `vsftpd` and so that the `httpd` restarts automatically with 30 seconds of failure.

---

19. Scheduling Tasks
    1. Ensure the `systemd timer` for cleaning up temporary files is enabled.
    2. Create a cron job which will update your system everyday at *11 p.m.*
    3. Schedule a task to reboot your machine at *4 a.m.* tomorrow.

---

20. Disk Encryption
    1. Create LUKS encrypted volume.
    2. Mount it persistently but not automatically.

---

21. Managing the Kernel
    1. Update the kernel (if available).
    2. Enable packet forwarding.

---

22. Managing the Boot Process
    1. Set the default target to multi-user.
    2. Set `GRUB` so that boot messages can be seen.
