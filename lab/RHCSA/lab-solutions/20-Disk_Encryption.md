# 20. Disk Encryption
> ###### Exam Objectives Covered
>> * 3.5: Configure systems to mount file systems at boot by universally unique ID (UUID) or label
> * 3.6: Add new partitions and logical volumes, and swap to a system non-destructively

---

## 20.1. Create Encrypted Volume
```sh
wipefs -fa /dev/vda
```

Use `fdisk` to create a single partition.

```sh
cryptsetup luksFormat /dev/vda1
cryptsetup luksOpen /dev/vda1 secrets

mkfs.xfs /dev/mapper/secrets

mkdir -p /data/secrets
```

## 20.2. Persistently Mount Encrypted Volume
```sh
vim /etc/fstab
# add following line:
# /dev/mapper/secrets     /data/secrets   xfs     defaults        0 0

blkid
# get UUID of block device

vim /etc/crypttab
# add following line:
secrets UUID="2bafbe34-828c-4770-84bc-b698a07ede85" none
```
