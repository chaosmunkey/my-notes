# 18. Scheduling Tasks
> ###### Exam Objectives Covered
> * 5.1: Schedule tasks using at and cron

---

## 1. Ensure `systemd timer` is Enabled
```sh
cd /usr/lib/systemd/system
ls *timer
# chrony-dnssrv@.timer  fstrim.timer           mdadm-last-resort@.timer  systemd-tmpfiles-clean.timer
# dnf-makecache.timer   insights-client.timer  mlocate-updatedb.timer    unbound-anchor.timer

ls systemd-tmpfiles-clean.*
# systemd-tmpfiles-clean.service  systemd-tmpfiles-clean.timer


systemctl enable --now systemd-tmpfiles-clean.timer
systemctl status systemd-tmpfiles-clean.timer
# ● systemd-tmpfiles-clean.timer - Daily Cleanup of Temporary Directories
#    Loaded: loaded (/usr/lib/systemd/system/systemd-tmpfiles-clean.timer; static; vendor preset: disabled)
#    Active: active (waiting) since Thu 2020-04-23 09:00:12 BST; 10min ago
#   Trigger: Thu 2020-04-23 09:15:08 BST; 4min 37s left
#      Docs: man:tmpfiles.d(5)
#            man:systemd-tmpfiles(8)

# Apr 23 09:00:12 server01.cmdl.dev systemd[1]: Started Daily Cleanup of Temporary Directories.
```


## 2. Create System Update *cron job*
```sh
crontab -e

# Add
0 23 * * * yum update -y
```

## 3. Reboot System Using *`at`*
```sh
systemctl status atd

at 4am tomorrow
# at> reboot
# at> <EOT>

atq
# 1	Thu Apr 23 04:00:00 2020 a root
```
