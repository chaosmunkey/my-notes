# 17. Managing Processes
> ###### Exam Objectives Covered
> * 2.4: Identify CPU/memory intensive processes and kill processes
> * 2.5: Adjust process scheduling
> * 2.6: Manage tuning profiles

## 1. Start `dd` Processes
```sh
nice -n -5 dd if=/dev/zero of=/dev/null
```
Press `Ctrl+z`, then type `bg`.

Type three time:
```sh
dd if=/dev/zero of=/dev/null &

jobs

# [1]   Running                 nice -n -5 dd if=/dev/zero of=/dev/null &
# [2]   Running                 dd if=/dev/zero of=/dev/null &
# [3]-  Running                 dd if=/dev/zero of=/dev/null &
# [4]+  Running                 dd if=/dev/zero of=/dev/null &
```

## 2. Using `top`
```sh
top
#  2076 root      15  -5    7328    928    864 R  99.3   0.0  11:25.87 dd
#  2082 root      20   0    7328    892    828 R  33.2   0.0   1:36.77 dd
#  2083 root      20   0    7328    808    744 R  33.2   0.0   1:36.19 dd
#  2084 root      20   0    7328    888    828 R  32.9   0.0   1:35.87 dd
```
Press `r`, *enter pid*, `-15`
```sh
# 2083 root       5 -15    7328    808    744 R  98.0   0.0   5:58.39 dd
# 2076 root      15  -5    7328    928    864 R  53.5   0.0  22:09.64 dd
# 2084 root      20   0    7328    888    828 R  24.9   0.0   5:11.83 dd
# 2082 root      20   0    7328    892    828 R  22.9   0.0   5:12.41 dd
```

Press `k`, *enter `top` pid*, `-9`

```sh
killall dd
```

## 3. Using `tuned`
```sh
tuned-adm active
# Current active profile: virtual-guest

tuned-adm list
# Available profiles:
# - balanced                    - General non-specialized tuned profile
# - desktop                     - Optimize for the desktop use-case
# - latency-performance         - Optimize for deterministic performance at the cost of increased power consumption
# - network-latency             - Optimize for deterministic performance at the cost of increased power consumption, focused on low latency network performance
# - network-throughput          - Optimize for streaming network throughput, generally only necessary on older CPUs or 40G+ networks
# - powersave                   - Optimize for low power consumption
# - throughput-performance      - Broadly applicable tuning that provides excellent performance across a variety of common server workloads
# - virtual-guest               - Optimize for running inside a virtual guest
# - virtual-host                - Optimize for running KVM guests


tuned-adm profile desktop
tuned-adm active
# Current active profile: desktop

tuned-adm profile virtual-guest
```
