# 2. Setup NFS Share
> ###### Exam Objectives
> * 2.9: Start, stop, and check the status of network services
> * 4.2: Mount and unmount network file systems using NFS
> * 5.2: Start and stop services and configure services to start automatically at boot
> * 5.5: Install and update software packages from Red Hat Network, a remote repository, or from the local file system
> * 6.3: Configure network services to start automatically at boot
> * 8.1: Configure firewall settings using firewall-cmd/firewalld

---

## 1. Setup NFS Share
```sh
yum install nfs-utils

mkdir -p /srv/nfs

cat > /etc/exports << _EOF_
/srv/nfs 192.168.123.0/24(rw,root_squash,sync,fsid=0)
_EOF_

# OPEN FIREWALL TO ALLOW NFS MOUNTING
firewall-cmd --permanent --add-service {nfs,rpc-bind,mountd}
firewall-cmd --reload

systemctl enable --now nfs-server

# VERIFICATION
systemctl status nfs-server
showmount -e

# ON REMOTE MACHINE
showmount -e <ip-address>
```


## 2. Mount NFS Share
```sh
yum install nfs-utils
showmount -e 192.168.123.254
# Export list for 192.168.123.254:
# /srv/nfs 192.168.123.0/24

mkdir -p /mnt/nfs
mount 192.168.123.254:/srv/nfs /mnt/nfs

# verify
df -h | grep nfs
# 192.168.123.254:/srv/nfs  15G     8.7G    6.4G    58%     /mnt/nfs

# Mount persistently
cat >> /etc/fstab << _EOF_
192.168.123.254:/srv/nfs        /mnt/nfs        nfs     _netdev        0 0
_EOF
```


## 3. Mount the Share Using `automount`
```sh
yum search autofs
# ==================================== Name Exactly Matched: autofs ====================================
# autofs.x86_64 : A tool for automatically mounting and unmounting filesystems
# autofs.x86_64 : A tool for automatically mounting and unmounting filesystems
# =================================== Summary & Name Matched: autofs ===================================
# libsss_autofs.x86_64 : A library to allow communication between Autofs and SSSD
# libsss_autofs.x86_64 : A library to allow communication between Autofs and SSSD

yum install autofs

systemctl enable --now autofs
```

### Examples...
```sh
cat /etc/auto.master
# #
# # Sample auto.master file
# # This is a 'master' automounter map and it has the following format:
# # mount-point [map-type[,format]:]map [options]
# # For details of the format look at auto.master(5).
# #
# /misc   /etc/auto.misc
# ...
```
Look at /etc/auto.misc...

```sh
# #
# # This is an automounter map and it has the following format
# # key [ -mount-options-separated-by-comma ] location
# # Details may be found in the autofs(5) manpage

# cd              -fstype=iso9660,ro,nosuid,nodev :/dev/cdrom

# # the following entries are samples to pique your imagination
# #linux          -ro,soft,intr           ftp.example.org:/pub/linux
# #boot           -fstype=ext2            :/dev/hda1
# ...
```
The **linux** example is a good example of an NFS mount.

Add the following line to */etc/auto.master*:
```sh
/nfs   /etc/auto.nfs
```

Create the file */etc/auto.nfs* and add the contents:
```sh
cat > /etc/auto.nfs << _EOF_
server01-srv    -rw     192.168.123.254:/srv/nfs
_EOF_

systemctl restart autofs.service

# verify
ls -a /nfs
# .  ..
```
Nothing?

```sh
cd /nfs/server01-srv
pwd
# /nfs/server01-srv

mount | grep "/srv/nfs"
# 192.168.123.254:/srv/nfs on /nfs/server01-srv type nfs (rw,relatime,vers=3,rsize=524288,wsize=524288,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,mountaddr=192.168.123.254,mountvers=3,mountport=20048,mountproto=udp,local_lock=none,addr=192.168.123.254)
```
