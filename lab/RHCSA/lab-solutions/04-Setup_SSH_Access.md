# 4. Setup SSH Access
> ###### Exam Objectives Covered
> * 8.3

## 1. Enable Key-Based Authentication
*On host machine:*
```sh
ssh-keygen -f ~/.ssh/rhcsa -t rsa -b 2048
# enter a passphrase to help protect you if you SSH key is compromised

ssh-copy-id -i ~/.ssh/rhcsa tom@192.168.123.254

# test
ssh tom@192.168.123.254 -i ~/.ssh/rhcsa
```

## 2. Disable Root Login
```sh
vim /etc/ssh/sshd_config
# VALUES TO CHANGE
PermitRootLogin no


systemctl restart sshd.service
```
