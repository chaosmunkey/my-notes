# 14. Stratis
> ###### Exam Objectives Covered
>> * 3.5: Configure systems to mount file systems at boot by universally unique ID (UUID) or label
> * 4.6: Manage layered storage
> * 5.2: Start and stop services and configure services to start automatically at boot

---

## 0. Pre Steps: Install `stratis` Packages
```sh
yum install stratis-cli stratisd
systemctl enable --now stratisd
```

## 1. Create Pools
### 1.1. Create the Pool
```sh
stratis pool create thepool /dev/vda

# verify
stratis pool list
# Name       Total Physical Size  Total Physical Used
# thepool                 10 GiB               52 MiB


# add device to pool
stratis pool add-data thepool /dev/vdb
stratis pool list
# Name       Total Physical Size  Total Physical Used
# thepool                 20 GiB               56 MiB

stratis blockdev
# Pool Name  Device Node    Physical Size   State  Tier
# thepool    /dev/vda              10 GiB  In-use  Data
# thepool    /dev/vdb              10 GiB  In-use  Data
```

## 2. Creating the Filesystems
```sh
stratis fs create thepool stratis1
stratis fs create thepool stratis2

stratis fs list
# Pool Name  Name      Used     Created            Device                     UUID
# thepool    stratis1  546 MiB  Apr 24 2020 09:03  /stratis/thepool/stratis1  7b81f194f8e84f919f77eb0c14015e22
# thepool    stratis2  546 MiB  Apr 24 2020 09:03  /stratis/thepool/stratis2  c30694ebd7204fb89e4dfe571413a363
```

### 2.1. `systemd` Mounting
```sh
mkdir -p /data/stratis1

cp /usr/lib/systemd/system/tmp.mount /etc/systemd/system/data-stratis1.mount
vim /etc/systemd/data-stratis1.mount

# contents:
# [Unit]
# Description=stratis1
# Conflicts=umount.target
# Before=local-fs.target umount.target
# Wants=stratisd.service

# [Mount]
# What=UUID="7b81f194-f8e8-4f91-9f77-eb0c14015e22"
# Where=/data/stratis1
# Type=xfs
# Options=defaults


systemctl daemon-reload
# verify it worked
systemctl list-unit-files | grep stratis
# data-stratis1.mount                         static
# stratisd.service                            enabled

systemctl enable --now data-stratis1.mount
systemctl status data-stratis1.mount
# ● data-stratis1.mount - stratis1
#    Loaded: loaded (/etc/systemd/system/data-stratis1.mount; static; vendor preset: disabled)
#    Active: active (mounted) since Fri 2020-04-24 09:29:50 BST; 11s ago
#     Where: /data/stratis1
#      What: /dev/mapper/stratis-1-4cf617afe0464a50b5bef17e66714c5c-thin-fs-7b81f194f8e84f919f77eb0c140>
#     Tasks: 0 (limit: 12537)
#    Memory: 12.0K
#    CGroup: /system.slice/data-stratis1.mount

# Apr 24 09:29:50 server01.cmdl.dev systemd[1]: Mounting stratis1...
# Apr 24 09:29:50 server01.cmdl.dev systemd[1]: Mounted stratis1.
```

### 2.2. `fstab` Mounting
```sh
mkdir -p /data/stratis2

vim /etc/fstab

# contents to add:
# UUID="c30694eb-d720-4fb8-9e4d-fe571413a363" /data/stratis2 xfs  defaults,x-systemd.requires=stratisd.service 0 0

mount -a
mount | grep stra
# /dev/mapper/stratis-1-4cf617afe0464a50b5bef17e66714c5c-thin-fs-c30694ebd7204fb89e4dfe571413a363 on /data/stratis2 type xfs (rw,relatime,seclabel,attr2,inode64,sunit=2048,swidth=2048,noquota,x-systemd.requires=stratisd.service)
```


## 3. Snapshots
```sh
stratis fs snapshot thepool stratis1 stratis1-snap

stratis fs list
# Pool Name  Name           Used     Created            Device                          UUID
# thepool    stratis1       546 MiB  Apr 24 2020 09:03  /stratis/thepool/stratis1       7b81f194f8e84f919f77eb0c14015e22
# thepool    stratis2       546 MiB  Apr 24 2020 09:03  /stratis/thepool/stratis2       c30694ebd7204fb89e4dfe571413a363
# thepool    stratis1-snap  546 MiB  Apr 24 2020 09:54  /stratis/thepool/stratis1-snap  e463a63a70fa4829a7575db19d0522c9

mount /stratis/thepool/stratis1-snap /mnt
```

## 4. Delete the Pool
Comment out the entry in `fstab` to prevent errors when booting.
```sh
systemctl disable --now data-stratis1.mount

stratis fs destroy thepool stratis1
stratis fs destroy thepool stratis2
stratis fs destroy thepool stratis1-snap
stratis pool destroy thepool
```
