# 5. Managing Software
> ###### Exam Objectives
> * 1.8: Create, delete, copy, and move files and directories
> * 5.5: Install and update software packages from Red Hat Network, a remote repository, or from the local file system

---

## 1. Create Local `YUM` Repo
### Mount ISO and copy files
*Assumes CD-ROM in **/dev/sr0***
```sh
mount -t iso9660 -o loop,ro /dev/sr0 /mount

mkdir -p /opt/repos

cp -r /mount/BaseOS /mount/AppStream /opt/repos
```

### Create the *yum repo* for **BaseOS**:
```sh
cat > /etc/yum.repos.d/baseos.repo << _EOF_
[BaseOS]
name=BaseOS Local
baseurl=file:///opt/repos/BaseOS
gpgcheck=0
_EOF_
```

### Create the *yum repo* for **AppStream**:
```sh
cat > /etc/yum.repos.d/appstream.repo << _EOF_
[AppStream]
name=AppStream Local
baseurl=file:///opt/repos/AppStream
gpgcheck=0
```

### Verification
```sh
yum repolist
# Not root, Subscription Management repositories not updated
# AppStream Local                                 136 MB/s | 5.6 MB     00:00
# BaseOS Local                                    152 MB/s | 2.2 MB     00:00
# repo id                           repo name                               status
# AppStream                         AppStream Local                         4,820
# BaseOS                            BaseOS Local                            1,661
```

---

## 2. Using the Local Repo - Part One
### 1. Searching The Repo
```sh
yum search sepolicy
# No matches found.

yum provides */sepolicy
# python3-policycoreutils-2.8-16.1.el8.noarch : SELinux policy core python3 interfaces
# Repo        : @System
# Matched from:
# Filename    : /usr/lib/python3.6/site-packages/sepolicy

# policycoreutils-devel-2.8-16.1.el8.i686 : SELinux policy core policy devel utilities
# Repo        : BaseOS
# Matched from:
# Filename    : /usr/bin/sepolicy
# Matched from:
# Filename    : /usr/share/bash-completion/completions/sepolicy

# policycoreutils-devel-2.8-16.1.el8.x86_64 : SELinux policy core policy devel utilities
# Repo        : BaseOS
# Matched from:
# Filename    : /usr/bin/sepolicy
# Matched from:
# Filename    : /usr/share/bash-completion/completions/sepolicy

# python3-policycoreutils-2.8-16.1.el8.noarch : SELinux policy core python3 interfaces
# Repo        : BaseOS
# Matched from:
# Filename    : /usr/lib/python3.6/site-packages/sepolicy
```

### 2. Query the *httpd* Package
```sh
yum download httpd

ls
# httpd-2.4.37-10.module+el8+2764+7127e69e.x86_64.rpm

rpm -qp --scripts httpd-2.4.37-10.module+el8+2764+7127e69e.x86_64.rpm
# warning: httpd-2.4.37-10.module+el8+2764+7127e69e.x86_64.rpm: Header V3 RSA/SHA256 Signature, key ID fd431d51: NOKEY
# postinstall scriptlet (using /bin/sh):

# if [ $1 -eq 1 ] ; then
#         # Initial installation
#         systemctl --no-reload preset httpd.service htcacheclean.service httpd.socket &>/dev/null || :
# fi
# preuninstall scriptlet (using /bin/sh):

# if [ $1 -eq 0 ] ; then
#         # Package removal, not upgrade
#         systemctl --no-reload disable --now httpd.service htcacheclean.service httpd.socket &>/dev/null || :
# fi
# postuninstall scriptlet (using /bin/sh):


# # Trigger for conversion from SysV, per guidelines at:
# # https://fedoraproject.org/wiki/Packaging:ScriptletSnippets#Systemd
# posttrans scriptlet (using /bin/sh):
# test -f /etc/sysconfig/httpd-disable-posttrans || \
#   /bin/systemctl try-restart --no-block httpd.service htcacheclean.service >/dev/null 2>&1 || :
```

---

## 3. Using AppStreams
### 1. Find Available *Perl* AppStreams
```sh
yum module list perl
# AppStream Local
# Name        Stream          Profiles                   Summary
# perl        5.24            common [d], minimal        Practical Extraction and Report Language
# perl        5.26 [d]        common [d], minimal        Practical Extraction and Report Language

# Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled
```

### 2. Install the Latest Minimal
```sh
yum module install perl:5.26/minimal

# Installed:
#   perl-interpreter-4:5.26.3-416.el8.x86_64         perl-Carp-1.42-396.el8.noarch
#   perl-Errno-1.28-416.el8.x86_64                   perl-Exporter-5.72-396.el8.noarch
#   perl-File-Path-2.15-2.el8.noarch                 perl-IO-1.38-416.el8.x86_64
#   perl-PathTools-3.74-1.el8.x86_64                 perl-Scalar-List-Utils-3:1.49-2.el8.x86_64
#   perl-Socket-4:2.027-2.el8.x86_64                 perl-Text-Tabs+Wrap-2013.0523-395.el8.noarch
#   perl-Unicode-Normalize-1.25-396.el8.x86_64       perl-constant-1.33-396.el8.noarch
#   perl-libs-4:5.26.3-416.el8.x86_64                perl-macros-4:5.26.3-416.el8.x86_64
#   perl-parent-1:0.237-1.el8.noarch                 perl-threads-1:2.21-2.el8.x86_64
#   perl-threads-shared-1.58-2.el8.x86_64

# Complete!
```

### 3. Downgrade Version of *Perl*
```sh
yum module remove perl
yum module reset perl
yum module install perl:5.24/minimal
```

### 4. Upgrade Version of *Perl*
```sh
yum module reset perl
yum module install perl:5.26/minimal
```
