# 7. Partitioning a Hard Drive Using the *`GPT` Partitioning Scheme*
> ###### Exam Objectives Covered
> * 2.1
> * 3.1
> * 3.5
> * 4.1

> ###### Assumptions
> * Unpartitioned disk at `/dev/vdc`

---

## Disk Info
Confirm `/dev/vdc` has no partitions.
```sh
lsblk /dev/vdc
# NAME MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
# vdc  252:32   0  10G  0 disk

gdisk -l /dev/vdc
# GPT fdisk (gdisk) version 1.0.3

# Partition table scan:
#   MBR: not present
#   BSD: not present
#   APM: not present
#   GPT: not present

# Creating new GPT entries.
# Disk /dev/vdc: 20971520 sectors, 10.0 GiB
# Sector size (logical/physical): 512/512 bytes
# Disk identifier (GUID): C685CA8D-2F89-4880-ACA0-3466F6CC3AB5
# Partition table holds up to 128 entries
# Main partition table begins at sector 2 and ends at sector 33
# First usable sector is 34, last usable sector is 20971486
# Partitions will be aligned on 2048-sector boundaries
# Total free space is 20971453 sectors (10.0 GiB)

# Number  Start (sector)    End (sector)  Size       Code  Name

```


## Partition the Disk
* Create *5GiB Primary Partition*
* Create *4GiB Primary Partition*
* Create *1GiB Extended Partition* For SWAP

```sh
gdisk /dev/vdc
# 5GiB Primary Partition
n # new partition
1 # parition number
# hit return to accept starting sector
+5G # 5Gib partition
8300 # Linux filesystem

# 4GiB Primary Partition
n # new partition
2 # parition number
# hit return to accept starting sector
+4G # 4Gib partition
8300 # Linux filesystem

# 1GiB Extended Partition
n # new partition
3 # parition number
# Hit return to accept defaults
8200 # Linux swap

w # write changes

partprobe
```

###### Verify
```sh
lsblk /dev/vdc
# NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
# vdc    252:32   0   10G  0 disk
# ├─vdc1 252:33   0    5G  0 part
# ├─vdc2 252:34   0    4G  0 part
# └─vdc3 252:35   0 1023M  0 part

parted -l /dev/vdc
# Model: Virtio Block Device (virtblk)
# Disk /dev/vdc: 10.7GB
# Sector size (logical/physical): 512B/512B
# Partition Table: gpt
# Disk Flags:

# Number  Start   End     Size    File system  Name              Flags
#  1      1049kB  5370MB  5369MB               Linux filesystem
#  2      5370MB  9665MB  4295MB               Linux filesystem
#  3      9665MB  10.7GB  1073MB               Linux swap        swap
```


## Format/Create File Systems
```sh
mkfs.xfs /dev/vdc1
# meta-data=/dev/vdc1              isize=512    agcount=4, agsize=327680 blks
#          =                       sectsz=512   attr=2, projid32bit=1
#          =                       crc=1        finobt=1, sparse=1, rmapbt=0
#          =                       reflink=1
# data     =                       bsize=4096   blocks=1310720, imaxpct=25
#          =                       sunit=0      swidth=0 blks
# naming   =version 2              bsize=4096   ascii-ci=0, ftype=1
# log      =internal log           bsize=4096   blocks=2560, version=2
#          =                       sectsz=512   sunit=0 blks, lazy-count=1
# realtime =none                   extsz=4096   blocks=0, rtextents=0

xfs_admin -L gpt-xfs /dev/vdc1
# writing all SBs
# new label = "gpt-xfs"

mkfs -t ext4 /dev/vdc2
# mke2fs 1.44.6 (5-Mar-2019)
# Discarding device blocks: done
# Creating filesystem with 1048576 4k blocks and 262144 inodes
# Filesystem UUID: d1bd18b4-29b3-4820-ba9a-c5b6dbc060f9
# Superblock backups stored on blocks:
# 	32768, 98304, 163840, 229376, 294912, 819200, 884736

# Allocating group tables: done
# Writing inode tables: done
# Creating journal (16384 blocks): done
# Writing superblocks and filesystem accounting information: done

e2label /dev/vdc2 gpt-ext4


mkswap /dev/vdc3
# Setting up swapspace version 1, size = 1023 MiB (1072668672 bytes)
# no label, UUID=7cf4addf-269a-4393-b164-44aa2c598ce0

swaplabel -L gpt-swap /dev/vdc3
```

###### Verify
```sh
lsblk -o NAME,SIZE,TYPE,FSTYPE,LABEL /dev/vdc
# NAME    SIZE TYPE FSTYPE LABEL
# vdc      10G disk
# ├─vdc1    5G part xfs    gpt-xfs
# ├─vdc2    4G part ext4   gpt-ext4
# └─vdc3 1023M part swap   gpt-swap

```


## Mounting - Temporarily
```sh
mkdir -p /mount/gpt-{xfs,ext4}

blkid
# /dev/vdc1: LABEL="gpt-xfs" UUID="4771acdf-3e75-4394-9261-e5596fbcfe3c" TYPE="xfs" PARTLABEL="Linux filesystem" PARTUUID="ab5f589f-8b31-4f72-8966-d48c6baab437"
# /dev/vdc2: LABEL="gpt-ext4" UUID="d1bd18b4-29b3-4820-ba9a-c5b6dbc060f9" TYPE="ext4" PARTLABEL="Linux filesystem" PARTUUID="9153ba7c-3373-4c01-a206-94ba74fd55ec"
# /dev/vdc3: LABEL="gpt-swap" UUID="7cf4addf-269a-4393-b164-44aa2c598ce0" TYPE="swap" PARTLABEL="Linux swap" PARTUUID="9af11eec-e25a-4593-a7b8-2946c84538b1"


mount UUID="4771acdf-3e75-4394-9261-e5596fbcfe3c" /mount/gpt-xfs
mount LABEL="gpt-ext4" /mount/gpt-ext4

# Pre-check off free memory
free -h
#               total        used        free      shared  buff/cache   available
# Mem:          1.8Gi       230Mi       1.3Gi       9.0Mi       251Mi       1.4Gi
# Swap:         2.0Gi          0B       2.0Gi

swapon LABEL="gpt-swap"
```

###### Verify
```sh
free -h
#               total        used        free      shared  buff/cache   available
# Mem:          1.8Gi       232Mi       1.3Gi       9.0Mi       251Mi       1.4Gi
# Swap:         3.0Gi          0B       3.0Gi

df -h
# Filesystem             Size  Used Avail Use% Mounted on
# /dev/vdc1              5.0G   68M  5.0G   2% /mount/gpt-xfs
# /dev/vdc2              3.9G   16M  3.7G   1% /mount/gpt-ext4
```


## Mounting - Permanently
```sh
cat >> /etc/fstab << _EOF_
UUID="4771acdf-3e75-4394-9261-e5596fbcfe3c" /mount/gpt-xfs     xfs     defaults    0 0
LABEL="gpt-ext4"                            /mount/gpt-ext4    ext4    defaults    0 0
LABEL="gpt-swap"                            swap               swap    defaults    0 0
_EOF_
```

###### Verify
```sh
mount -a
# should return no errors
reboot

# ... wait for server to reboot ...
df -h
# Filesystem             Size  Used Avail Use% Mounted on
# /dev/vdc1              5.0G   68M  5.0G   2% /mount/gpt-xfs
# /dev/vdc2              3.9G   16M  3.7G   1% /mount/gpt-ext4

free -h
#               total        used        free      shared  buff/cache   available
# Mem:          1.8Gi       231Mi       1.4Gi       9.0Mi       197Mi       1.4Gi
# Swap:         3.0Gi          0B       3.0Gi
```
