# 12. Setup NTP Server
> ###### Exam Objectives Covered
>* 5.2: Start and stop services and configure services to start automatically at boot
>* 5.4: Configure time service clients
>* 5.5: Install and update software packages from Red Hat Network, a remote repository, or from the local file system
>* 6.3: Configure network services to start automatically at boot
>* 8.1: Configure firewall settings using firewall-cmd/firewalld

---

## a. Setup NTP Server
```sh
yum install chrony

vim /etc/chrony.conf
# CONTENTS TO ADD
allow 192.168.123.0/24
stratum 8
# CONTENTS TO COMMENT OUT
pool 2.rhel.pool.ntp.org iburst
# SAVE AND EXIT VIM


firewall-cmd --permanent --add-service ntp
firewall-cmd --reload

systemctl enable --now chronyd
```

## b. Sync to NTP Server
```sh
vim /etc/chrony.conf
# CONTENTS TO COMMENT OUT
pool 2.rhel.pool.ntp.org iburst
# CONTENTS TO ADD
server 192.168.123.254
# SAVE AND EXIT VIM

systemctl enable --now chronyd.service

# verify
chronyc sources
# 210 Number of sources = 1
# MS Name/IP address         Stratum Poll Reach LastRx Last sample
# ===============================================================================
# ^? 192.168.123.254               8   6     1    17   -277ms[ -277ms] +/-  655us
```
