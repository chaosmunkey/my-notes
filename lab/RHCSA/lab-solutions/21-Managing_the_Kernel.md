# 21. Managing the Kernel


## 21.1. Update Kernel
```sh
yum update kernel
```

## 21.2. Enable Packet Forwarding
```sh
sysctl -a | grep forward
# net.ipv4.conf.all.forwarding = 0
# net.ipv4.conf.all.mc_forwarding = 0
# net.ipv4.conf.default.forwarding = 0
# net.ipv4.conf.default.mc_forwarding = 0
# net.ipv4.conf.ens3.forwarding = 0
# net.ipv4.conf.ens3.mc_forwarding = 0
# net.ipv4.conf.ens4.forwarding = 0
# net.ipv4.conf.ens4.mc_forwarding = 0
# net.ipv4.conf.lo.forwarding = 0
# net.ipv4.conf.lo.mc_forwarding = 0
# net.ipv4.ip_forward = 0
# net.ipv4.ip_forward_use_pmtu = 0
# net.ipv6.conf.all.forwarding = 0
# net.ipv6.conf.all.mc_forwarding = 0
# net.ipv6.conf.default.forwarding = 0
# net.ipv6.conf.default.mc_forwarding = 0
# net.ipv6.conf.ens3.forwarding = 0
# net.ipv6.conf.ens3.mc_forwarding = 0
# net.ipv6.conf.ens4.forwarding = 0
# net.ipv6.conf.ens4.mc_forwarding = 0
# net.ipv6.conf.lo.forwarding = 0
# net.ipv6.conf.lo.mc_forwarding = 0
```
Interested in the `net.ipv4.ip_forward` parameter.

```sh
cd /proc/sys/net/ipv4
echo 1 > ip_forward

# verify
cat ip_forward
# 1

sysctl -a | grep forward
# ...
# net.ipv4.ip_forward = 1
# ...
```
Make it permanent:
```sh
echo "net.ipv4.ip_forward = 1" > /etc/sysctl.d/ipv4.conf
systctl -p /etc/sysctl.d/ipv4.conf

reboot
## wait for reboot
sysctl -a | grep forward
# ...
# net.ipv4.ip_forward = 1
# ...
```
