# 22. Managing the Boot Process
> ###### Exam Objectives Covered
> * 2.1: Boot, reboot, and shut down a system normally
> * 2.2: Boot systems into different targets manually
> * 5.3: Configure systems to boot into a specific target automatically
> * 5.7: Modify the system bootloader

---

## 22.1 Setting Default Target
```sh
systemctl get-default
# graphical.target

systemctl set-default multi-user.target

# verify
reboot
```

## 22.2 Display Boot Messages
```sh
vim /etc/default/grub
```

Locate the *GRUB_CMDLINE_LINUX* line, and remove `rhgb` and `quiet` from the string.

**IF uefi**:
```sh
grub2-mkconfig -o /boot/efi/EFI/redhat/grub.cfg
```

**ELSE**:
```sh
grub2-mkconfig -o /boot/grub2/grub.cfg
```

```sh
reboot
```

## 22.3 Manual Target
Once at the `GRUB` boot menu, press `e`. Add `systemd.unit=emergency.target` to the end of the line which starts with *`linux`*. Enter *root* password when prompted.

```sh
systemctl isolate rescue.target
```
Enter *root* password when prompted.

```sh
systemctl start multi-user.target
```
