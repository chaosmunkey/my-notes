# 3. Network Interfaces
> ###### Exam Objectives Covered
> * 6.1: Configure IPv4 and IPv6 addresses
> * 6.2: Configure hostname resolution


> ###### Assumptions
> `ens10` is the spare NIC.

---

## 1. Setup Temporary Address
```sh
ip link
# 1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
#     link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
# 2: ens3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
#     link/ether 52:54:00:45:bc:69 brd ff:ff:ff:ff:ff:ff
# 3: ens10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
#     link/ether 52:54:00:24:9e:63 brd ff:ff:ff:ff:ff:ff

ip addr
# 1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
#     link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
#     inet 127.0.0.1/8 scope host lo
#        valid_lft forever preferred_lft forever
#     inet6 ::1/128 scope host
#        valid_lft forever preferred_lft forever
# 2: ens3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
#     link/ether 52:54:00:45:bc:69 brd ff:ff:ff:ff:ff:ff
#     inet 192.168.123.254/24 brd 192.168.123.255 scope global noprefixroute ens3
#        valid_lft forever preferred_lft forever
#     inet6 fe80::cb44:3dcf:41f8:c1d1/64 scope link noprefixroute
#        valid_lft forever preferred_lft forever
# 3: ens10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
#     link/ether 52:54:00:24:9e:63 brd ff:ff:ff:ff:ff:ff
```
`ens10` doesn't have an address assigned, shall use this one.

```sh
ip addr add dev ens10 192.168.124.254/24
ip addr sh ens10
# 3: ens10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
#     link/ether 52:54:00:24:9e:63 brd ff:ff:ff:ff:ff:ff
#     inet 192.168.124.254/24 scope global ens10
#        valid_lft forever preferred_lft forever

# verify connectivity
ping 192.168.124.1 -c 1
# PING 192.168.124.1 (192.168.124.1) 56(84) bytes of data.
# 64 bytes from 192.168.124.1: icmp_seq=1 ttl=64 time=0.473 ms

# --- 192.168.124.1 ping statistics ---
# 1 packets transmitted, 1 received, 0% packet loss, time 0ms
# rtt min/avg/max/mdev = 0.473/0.473/0.473/0.000 ms

ping 8.8.8.8 -c 1
# connect: Network is unreachable
```
Can't ping outside of the network.

```sh
ip route show
# 192.168.123.0/24 dev ens3 proto kernel scope link src 192.168.123.254 metric 100
# 192.168.124.0/24 dev ens10 proto kernel scope link src 192.168.124.254
```
No default route.

```sh
ip route add default via 192.168.124.1

# verify
ip route show
# default via 192.168.124.1 dev ens10
# 192.168.123.0/24 dev ens3 proto kernel scope link src 192.168.123.254 metric 100
# 192.168.124.0/24 dev ens10 proto kernel scope link src 192.168.124.254

ping 8.8.8.8 -c 1
# PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
# 64 bytes from 8.8.8.8: icmp_seq=1 ttl=52 time=25.5 ms

# --- 8.8.8.8 ping statistics ---
# 1 packets transmitted, 1 received, 0% packet loss, time 0ms
# rtt min/avg/max/mdev = 25.543/25.543/25.543/0.000 ms
```

### Reboot Server
```sh
reboot

ip add sh ens10
# 3: ens10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    # link/ether 52:54:00:24:9e:63 brd ff:ff:ff:ff:ff:ff
```

## 2. DHCP Network Profile
```sh
nmcli con add con-name ens10-dhcp ifname ens10 autoconnect no type ethernet
nmcli con up ens10-dhcp
# Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/2)

# verify
nmcli con show
# NAME             UUID                                  TYPE      DEVICE
# ens10-dhcp       429bfbc9-e7f2-4ecf-b277-3b90dbc8bbda  ethernet  ens10
# isolated-static  e7b6d5a0-1099-4292-9e91-413cfa3b5722  ethernet  ens3
# static-sec       363f7a22-ad88-451e-919e-4e7c3ba97871  ethernet  --

ip add sh ens10
# 3: ens10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
#     link/ether 52:54:00:24:9e:63 brd ff:ff:ff:ff:ff:ff
#     inet 192.168.124.15/24 brd 192.168.124.255 scope global dynamic noprefixroute ens10
#        valid_lft 3556sec preferred_lft 3556sec
#     inet6 fe80::61be:77a:83f0:84e7/64 scope link noprefixroute
#        valid_lft forever preferred_lft forever
```

## 3. Static IPv4 and IPv6 Address
```sh
nmcli con add con-name ens10-static \
    ifname ens10 type ethernet autoconnect yes \
    ipv4.addresses 192.168.124.254/24 \
    ipv4.gateway 192.168.124.1 \
    ipv4.dns 192.168.124.1 \
    ipv4.method manual
# Connection 'ens10-static' (b23980f1-3356-4164-bf19-2b9d2e7e4c8f) successfully added.
nmcli con up ens10-static
# Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/3)
```


### Verify Connection
```sh
nmcli con show
# NAME             UUID                                  TYPE      DEVICE
# ens10-static     b23980f1-3356-4164-bf19-2b9d2e7e4c8f  ethernet  ens10
# isolated-static  e7b6d5a0-1099-4292-9e91-413cfa3b5722  ethernet  ens3
# ens10-dhcp       429bfbc9-e7f2-4ecf-b277-3b90dbc8bbda  ethernet  --
# static-sec       363f7a22-ad88-451e-919e-4e7c3ba97871  ethernet  --

ip addr sh ens10
# 3: ens10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
#     link/ether 52:54:00:24:9e:63 brd ff:ff:ff:ff:ff:ff
#     inet 192.168.124.254/24 brd 192.168.124.255 scope global noprefixroute ens10
#        valid_lft forever preferred_lft forever
#     inet6 fe80::8753:b735:d564:7232/64 scope link noprefixroute
#        valid_lft forever preferred_lft forever

host bbc.co.uk
# bbc.co.uk has address 151.101.0.81
# bbc.co.uk has address 151.101.192.81
# bbc.co.uk has address 151.101.128.81
# bbc.co.uk has address 151.101.64.81
# bbc.co.uk has IPv6 address 2a04:4e42::81
# bbc.co.uk has IPv6 address 2a04:4e42:600::81
# bbc.co.uk has IPv6 address 2a04:4e42:400::81
# bbc.co.uk has IPv6 address 2a04:4e42:200::81
# bbc.co.uk mail is handled by 10 cluster1.eu.messagelabs.com.
# bbc.co.uk mail is handled by 20 cluster1a.eu.messagelabs.com.

ping bbc.co.uk -c1
# PING bbc.co.uk (151.101.64.81) 56(84) bytes of data.
# 64 bytes from 151.101.64.81 (151.101.64.81): icmp_seq=1 ttl=55 time=27.9 ms

# --- bbc.co.uk ping statistics ---
# 1 packets transmitted, 1 received, 0% packet loss, time 0ms
# rtt min/avg/max/mdev = 27.885/27.885/27.885/0.000 ms

reboot

# wait for server to reboot...
ip addr sh ens10
# 3: ens10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
#     link/ether 52:54:00:24:9e:63 brd ff:ff:ff:ff:ff:ff
#     inet 192.168.124.254/24 brd 192.168.124.255 scope global noprefixroute ens10
#        valid_lft forever preferred_lft forever
#     inet6 fe80::8753:b735:d564:7232/64 scope link noprefixroute
#        valid_lft forever preferred_lft forever

```

