# 3. Create New Users
> ###### Exam Objectives Covered
> * 7.1
> * 7.2
> * 7.3
> * 7.4

## 1. Create Groups
```sh
groupadd operations
groupadd accounts
groupadd developers -g 1200
```

## 2. Create `tom` User
```sh
adduser tom
usermod -aG wheel
```

## 3. Create `jacob` User
```sh
useradd jacob -u 1234 -G operations
passwd -w 7 -x 30 jacob

# verify
id jacob
# uid=1234(jacob) gid=1234(jacob) groups=1234(jacob),1002(operations)

chage -l jacob
# Last password change                              : Mar 02, 2020
# Password expires                                  : Apr 01, 2020
# Password inactive                                 : never
# Account expires                                   : never
# Minimum number of days between password change    : 0
# Maximum number of days between password change    : 30
# Number of days of warning before password expires : 7
```


## 4. Create `susan`, `jessica` and `steve` Users
```sh
vim /etc/login.defs
# VALUES TO CHANGE
PASS_MAX_DAYS    120
PASS_MIN_LEN     5
PASS_WARN_AGE    3
UID_MIN          1500
GID_MIN          1500


adduser jessica -G developers
adduser steve -G developers
adduser susan -G accounts

# verify with one account
id jessica
# uid=1500(jessica) gid=1502(jessica) groups=1502(jessica),1200(developers)

chage -l jessica
# Last password change                              : Mar 03, 2020
# Password expires                                  : Jul 01, 2020
# Password inactive                                 : never
# Account expires                                   : never
# Minimum number of days between password change    : 0
# Maximum number of days between password change    : 120
# Number of days of warning before password expires : 3
```
