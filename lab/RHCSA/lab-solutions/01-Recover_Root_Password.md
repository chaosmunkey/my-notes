# 1. Reset *forgotten* **root** Password
1. (Re)boot system, and press `e` when the `GRUB` menu is displayed.

2. On the line where the `kernel` is specified, add to the end `rd.break`, and press `Ctrl+x` to boot.

3. Remount the `/sysroot` directory as `rw`:
```sh
mount -o remount,rw /sysroot
```

4. Set `/sysroot` as the new root directory:
```sh
chroot /sysroot
```

5. (Re)set the **root** password: `passwd`

6. Reset the **SELinux** context on the *etc/shadow* file:
```sh
load_policy -i
chcon -t shadow_t /etc/shadow
```

*Alternatively:* Alternative but slower:
```sh
touch /.autorelabel
```

7. Exit `chroot` and reboot.
