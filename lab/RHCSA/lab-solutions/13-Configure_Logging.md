# 13. Configure Logging
> ###### Exam Objectives
> * 1.8: Create, delete, copy, and move files and directories
> * 2.7: Locate and interpret system log files and journals
> * 2.8: Preserve system journals

---

## 1. Ensure `journald` is Persistently Logging
```sh
mkdir -p /var/log/journal
systemctl restart systemd-journald

# verify
systemctl status systemd-journald
# ● systemd-journald.service - Journal Service
#    Loaded: loaded (/usr/lib/systemd/system/systemd-journald.service; static; vendor preset: disabled)
#    Active: active (running) since Mon 2020-03-09 17:51:37 GMT; 10s ago
#      Docs: man:systemd-journald.service(8)
#            man:journald.conf(5)
#  Main PID: 2075 (systemd-journal)
#    Status: "Processing requests..."
#     Tasks: 1 (limit: 23975)
#    Memory: 1.3M
#    CGroup: /system.slice/systemd-journald.service
#            └─2075 /usr/lib/systemd/systemd-journald

# Mar 09 17:51:37 server01.cmdl.dev systemd-journald[2075]: Journal started
# Mar 09 17:51:37 server01.cmdl.dev systemd-journald[2075]: System journal (/var/log/journal/e963fc3717d146e9b64bc84c82f63da3) is 8.0M, max 3.5G, 3.4G free.
```

## 2. Create an Entry in `rsyslog`
```sh
vim /etc/rsyslog.conf

# add line
*.error /var/log/error


systemctl restart rsyslog.service
```

## 3. Rotate **/var/log/error** Log File
```sh
cat > /etc/logrotate.d/error << _EOF_
/var/log/error {
    missingok
    daily
    rotate 7
    compress
}
_EOF_
```
