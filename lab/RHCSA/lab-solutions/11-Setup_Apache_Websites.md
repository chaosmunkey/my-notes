# 5. Setup Apache Website
> ###### Exam Objectives Covered
>* 5.2
>* 5.5
>* 5.6
>* 6.3
>* 8.1
>* 8.4
>* 8.8

## 1. Default Installation
```sh
yum search httpd
yum module install httpd
firewall-cmd --permanent --add-service=http
firewall-cmd --reload

systemctl enable --now httpd
```

## 2. Non-Standard Location, Non-Standard Port
**_TODO_: FINISH THIS; Add more context to location**
```sh
# Initially set SELinux into permissive mode for testing.
setenforce 0

mkdir -p /srv/www/{logs,html}

cat > /srv/www/html/index.html << _EOF_
<html>
    <head>
        <title>Test Page...</title>
    </head>
    <body>
        <h1>Welcome to the alternative page...</h1>
    </body>
</html>
_EOF_

cat > /etc/httpd/conf.d/test.cmdl.dev.conf << _EOF_
Listen 101

<VirtualHost *:101>
    DocumentRoot "/srv/www/html"
    ServerName test.cmdl.dev
    ErrorLog "/srv/www/logs/error.log"
    CustomLog "/srv/www/logs/custom.log" combined
    <Directory /srv/www>
        Require all granted
    </Directory>
</VirtualHost>
_EOF_

firewall-cmd --permanent --add-port=101/tcp
firewall-cmd --reload

systemctl restart httpd

# TESTING
# Locally
curl localhost
curl localhost:101

# Remotely
curl 192.168.123.254
curl 192.168.123.254:101
```

#### Fixing SELinux Problems
##### Non-Standard Port
```sh
# re-enable SELinux
setenforce 1

systemctl restart httpd
```
`systemctl` should now complain that it was unable to start the `httpd` daemon, the reason for this can be found if you run a particular command:
```sh
journalctl -xe

    setroubleshoot[1941]: SELinux is preventing /usr/sbin/httpd from name_bind access on the tcp_socket port 101. For complete SELinux messages run: sealert -l 0f973530-3ee6-426e-a6ae-a6238fab3cd6
    platform-python[1941]: SELinux is preventing /usr/sbin/httpd from name_bind access on the tcp_socket port 101.

        *****  Plugin bind_ports (99.5 confidence) suggests   ************************

        If you want to allow /usr/sbin/httpd to bind to network port 101
        Then you need to modify the port type.
        Do
        # semanage port -a -t PORT_TYPE -p tcp 101
            where PORT_TYPE is one of the following: http_cache_port_t, http_port_t, jboss_management_port_t, jboss_messaging_port_t, ntop_port_t, puppet_port_t.

        *****  Plugin catchall (1.49 confidence) suggests   **************************

        If you believe that httpd should be allowed name_bind access on the port 101 tcp_socket by default.
        Then you should report this as a bug.
        You can generate a local policy module to allow this access.
        Do
        allow this access for now by executing:
        # ausearch -c 'httpd' --raw | audit2allow -M my-httpd
        # semodule -X 300 -i my-httpd.pp
```
Within this output, there's a mention of a couple of commands. The `sealert` command will print out a more detailed report of what went wrong.
However, we can simply run the `semanage` command that's listed; this is a `http_port_t` type.
```sh
semanage port -a -t http_port_t -p tcp 101

systemctl restart httpd

# TESTING
# Locally
curl localhost
curl localhost:101
```
When testing the site listening on port *101*, the default page is displayed, looks like there's still another SELinux issue to fix.

##### File Contexts
Get the current *file context* of the **/var/www/** directory:
```sh
ls -Z /var/www
    system_u:object_r:httpd_sys_script_exec_t:s0 cgi-bin
        system_u:object_r:httpd_sys_content_t:s0 html
```
**/var/www/html** has the SELinux type of `httpd_sys_content_t`, this will also need to be applied to the **/srv/www/html** directory:
```sh
semanage fcontext -a -t httpd_sys_content_t "/srv/www/html(/.*)?"
# Make sure the changes persist reboots...
restorecon -Rv /srv/www/html
```
