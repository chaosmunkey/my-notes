# 15. `vdo` Volumes
> ###### Exam Objectives Covered
>

## 1. Install `vdo` Packages
```sh
yum install vdo kmod-kvdo
```

## 2.  Create the `vdo`
```sh
#  ensure disk has no signatures from previous uses
wipefs --all /dev/vdc

vdo create --name=a-vdo --device=/dev/vdc --vdoLogicalSize=1T
# Creating VDO a-vdo
# Starting VDO a-vdo
# Starting compression on VDO a-vdo
# VDO instance 0 volume is ready at /dev/mapper/a-vdo

mkfs.xfs -K /dev/mapper/a-vdo
# meta-data=/dev/mapper/a-vdo      isize=512    agcount=4, agsize=67108864 blks
#          =                       sectsz=4096  attr=2, projid32bit=1
#          =                       crc=1        finobt=1, sparse=1, rmapbt=0
#          =                       reflink=1
# data     =                       bsize=4096   blocks=268435456, imaxpct=5
#          =                       sunit=0      swidth=0 blks
# naming   =version 2              bsize=4096   ascii-ci=0, ftype=1
# log      =internal log           bsize=4096   blocks=131072, version=2
#          =                       sectsz=4096  sunit=1 blks, lazy-count=1
# realtime =none                   extsz=4096   blocks=0, rtextents=0

udevadm settle
```

## 3. Mount the `vdo`
```sh
mkdir -p /mount/vdo

vim /etc/fstab
# add the following line
/dev/mapper/a-vdo       /mount/vdo      xfs     x-systemd.requires=vdo.service  0 0


mount -a


# verify
vdostats --human-readable
# Device                    Size      Used Available Use% Space saving%
# /dev/mapper/a-vdo        10.0G      4.0G      6.0G  40%           99%

df -h /mount/vdo
# Filesystem         Size  Used Avail Use% Mounted on
# /dev/mapper/a-vdo  1.0T  7.2G 1017G   1% /mount/vdo

```
