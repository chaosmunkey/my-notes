# 18. Working with `systemd`
> ###### Exam Objectives Covered
> * 5.2: Start and stop services and configure services to start automatically at boot

## 1. Install Packages
```sh
yum install httpd vsftpd
systemctl enable --now httpd
```

## 2. Set `systemctl` Default Editor
```sh
man systemctl
```
Search for `editor`, should make reference to the `Environment` section.

```sh
# systemctl show-environment
# # LANG=en_GB.UTF-8
# # PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin

# systemctl set-environment SYSTEMD_EDITOR=$(which vim)

# systemctl show-environment
# # SYSTEMD_EDITOR=/usr/bin/vim
# # LANG=en_GB.UTF-8
# # PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin

echo "export SYSTEMD_EDITOR=$(which vim)" >> /root/.bash_profile
```

## 3. Editing `httpd` service
Use `systemctl show httpd` to see the options available for options.

```sh
systemctl edit httpd

# [Unit]
# Requires=vsftpd.service

# [Service]
# Restart=always
# RestartSec=30s

systemctl start httpd
systemctl status httpd
# ● httpd.service - The Apache HTTP Server
#    Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
#   Drop-In: /etc/systemd/system/httpd.service.d
#            └─override.conf
#    Active: active (running) since Wed 2020-04-22 10:16:48 BST; 6s ago
#      Docs: man:httpd.service(8)
#  Main PID: 2506 (httpd)

systemctl status vsftpd
# ● vsftpd.service - Vsftpd ftp daemon
#    Loaded: loaded (/usr/lib/systemd/system/vsftpd.service; disabled; vendor preset: disabled)
#    Active: active (running) since Wed 2020-04-22 10:16:48 BST; 1min 6s ago
#   Process: 2507 ExecStart=/usr/sbin/vsftpd /etc/vsftpd/vsftpd.conf (code=exited, status=0/SUCCESS)
#  Main PID: 2508 (vsftpd)


pkill httpd

watch systemctl status httpd
```

`httpd` should go into an *`activating`* state, before restarting after 30 seconds.
