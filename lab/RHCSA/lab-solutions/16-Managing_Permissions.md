# 16. Managing Permissions
> ###### Exam Objectives Covered
> * 1.2: Use input-output redirection (>, >>, |, 2>, etc.)
> * 1.5: Log in and switch users in multiuser targets
> * 1.7: Create and edit text files
> * 1.8: Create, delete, copy, and move files and directories
> * 1.10: List, set, and change standard ugo/rwx permissions
> * 4.4: Create and configure set-GID directories for collaboration
> * 7.3: Create, delete, and modify local groups and group memberships

---

## 1. Ensure *other* Users Have No Default Permissions `tom` Creates
Edit the `umask` in the *.bash_rc* file in **tom**'s home directory.
```sh
vim ~tom/.bash
# add
umask 007
```

###### Verify
```sh
su - tom
touch a_file
ls -l a_file
# -rw-rw----. 1 tom tom 0 Mar 15 18:53 a_file
```

## 2. Create Directories for the `accounts`, `developers` and `operations` Group
```sh
mkdir -p /data/{accounts,developers,operations}
```

### a. Group Members Have Full Access
```sh
for grp in {accounts,developers,operations};
do
    chgrp "${grp}" "/data/${grp}"
    chmod 0770 "/data/${grp}"
done

# Verify
ls -l /data
# total 0
# drwxrwx---. 2 root accounts   6 Mar 15 18:55 accounts
# drwxrwx---. 2 root developers 6 Mar 15 18:55 developers
# drwxrwx---. 2 root operations 6 Mar 15 18:55 operations
```

### b. Files Created are Writeable For Group Members
```sh
chmod -R u+s /data/*
# OR: chmod -R 2770 /data/*

# verify
ls -l /data
# total 0
# drwxrws---. 2 root accounts   6 Mar 15 18:55 accounts
# drwxrws---. 2 root developers 6 Mar 15 18:55 developers
# drwxrws---. 2 root operations 6 Mar 15 18:55 operations

su - jessica
cd /data/developers
touch file
ls -l
# -rw-rw-r--. 1 jessica developers  0 Mar 16 18:45 file

## LOGOUT jessica

su - steve
cd /data/developers
echo "hello" >> file
cat file
# hello
```

### c. Users Can Only Delete Files They Created
```sh
chmod -R +t /data/*
# OR: chmod -R 3770 /data/*

# verify
ls -l /data
# drwxrws--T. 2 root accounts    6 Mar 15 18:55 accounts
# drwxrws--T. 2 root developers 18 Mar 16 18:45 developers
# drwxrws--T. 2 root operations  6 Mar 15 18:55 operations

su - steve
rm /data/developers/file
# rm: cannot remove '/data/developers/file': Operation not permitted

## LOGOUT steve

su - jessica
rm /data/developers/file
ls -l /data/developers/
# total 0
```

## d. `operations` Have Read Access to `developers` Directory
###### Setup *operations* directory.
```sh
su - jessica

cat > /data/developers/dev.txt << _EOF_
This is a file owned by the developers group.
_EOF_

## LOGOUT jessica
su - jacob
cd /data/developers/
# -bash: cd: /data/developers/: Permission denied
```

```sh
cd /data

getfacl developers/
# # file: developers/
# # owner: root
# # group: developers
# # flags: -st
# user::rwx
# group::rwx
# other::---


setfacl -m d:g:operations:rx developers

getfacl developers
# # file: developers
# # owner: root
# # group: developers
# # flags: -st
# user::rwx
# group::rwx
# other::---
# default:user::rwx
# default:group::rwx
# default:group:operations:r-x
# default:mask::rwx
# default:other::---
```
The file created in the earlier step, didn't have any **ACL**s assigned to it:
```sh
ls -al /data/developers/dev.txt
# -rw-rw-r--. 1 jessica developers 46 Mar 21 18:03 dev.txt

setfacl -R -m g:operations:rx /data/developers
```

```sh

# verify
su - jacob
cat /data/developers/dev.txt
# This is a file owned by the developers group..
```

## e. CEO Can Delete Everything in */data/*
```sh
chown -R /data/*

# verify
su - tom
rm /data/developers/dev.txt
# rm: remove write-protected regular file 'dev.txt'? y
ls
```
