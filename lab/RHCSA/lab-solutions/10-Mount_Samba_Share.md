# 10. Setup a Samba Share
> ###### Exam Objectives Covered
> * 1.10
> * 2.9
> * 4.2
> * 5.2
> * 5.5
> * 6.3
> * 7.1
> * 8.1


## Creating the Samba Share
> Not this section is not required for the RHCSA exam, but it's good to know...

```sh
yum install samba

useradd samba
# don't set the password for this account as we don't want people logging in...
# However, for Windoze users to be able to authenticate we must use `smbpasswd`
smbpasswd -a samba
# New SMB password:
# Retype new SMB password:
# Added user samba.

mkdir -p /srv/samba
chown samba /srv/samba/
chmod 0770 /srv/samba/

```

Ensure the following lines are added to `/etc/samba/smb.conf`:
```
[samba]
        comment = samba share
        path = /srv/samba
        write list = samba
```

```sh
systemctl enable --now smb
# verify
systemctl status smb
testparm
# Load smb config files from /etc/samba/smb.conf
# rlimit_max: increasing rlimit_max (1024) to minimum Windows limit (16384)
# Processing section "[homes]"
# Processing section "[printers]"
# Processing section "[print$]"
# Processing section "[samba]"
# Loaded services file OK.
# Server role: ROLE_STANDALONE

# Press enter to see a dump of your service definitions
# ...


firewall-cmd --permanent --add-service samba
firewall-cmd --reload
# verify
firewall-cmd --list-all
```

## Mounting the Samba Share
```sh
yum groups list --hidden | grep -i network
# Common NetworkManager submodules
# Dial-up Networking Support
# Network File System Client
# Network Servers
# Networking Tools

yum groups install "Network File System Client"

# verify server mount
smbclient -L //192.168.123.254
# Enter SAMBA\root's password:
# Anonymous login successful

#   Sharename       Type      Comment
#   ---------       ----      -------
#   print$          Disk      Printer Drivers
#   samba           Disk      samba share
#   IPC$            IPC       IPC Service (Samba 4.9.1)
# Reconnecting with SMB1 for workgroup listing.
# Anonymous login successful

#   Server               Comment
#   ---------            -------

#   Workgroup            Master
#   ---------            -------


mount -o username=samba //192.168.123.254/samba /mnt
# Password for samba@//192.168.123.254/samba:  *************
mount | grep samba
# //192.168.123.254/samba on /mnt type cifs (rw,relatime,vers=default,cache=strict,username=samba,domain=,uid=0,noforceuid,gid=0,noforcegid,addr=192.168.123.254,file_mode=0755,dir_mode=0755,soft,nounix,serverino,mapposix,rsize=1048576,wsize=1048576,echo_interval=60,actimeo=1)

```

Add the following line to `/etc/fstab`:
```
//192.168.123.254/samba /srv/samba  cifs    _netdev,username=samba,password=  0 0
```
> enter password, this isn't the most secure of methods....

```sh
mount -a
# verify
mount | grep samba
# //192.168.123.254/samba on /srv/samba type cifs (rw,relatime,vers=default,cache=strict,username=samba,domain=,uid=0,noforceuid,gid=0,noforcegid,addr=192.168.123.254,file_mode=0755,dir_mode=0755,soft,nounix,serverino,mapposix,rsize=1048576,wsize=1048576,echo_interval=60,actimeo=1,_netdev)

reboot
```

---

## Troubleshooting
In this current form, anytime the client attempts to modify files on the server, the following error is produced:
```sh
touch: cannot touch 'file': Permission denied
```

Looking at the `/var/log/audit/audit.log` on the server, the following error is produced:
```
type=AVC msg=audit(1583053020.069:110): avc:  denied  { create } for  pid=1971 comm="smbd" name="file" scontext=system_u:system_r:smbd_t:s0 tcontext=system_u:object_r:var_t:s0 tclass=file permissive=0
type=SYSCALL msg=audit(1583053020.069:110): arch=c000003e syscall=257 success=no exit=-13 a0=ffffff9c a1=5626e3c453c0 a2=200c2 a3=1e4 items=0 ppid=1677 pid=1971 auid=4294967295 uid=1001 gid=0 euid=1001 suid=0 fsuid=1001 egid=1001 sgid=0 fsgid=1001 tty=(none) ses=4294967295 comm="smbd" exe="/usr/sbin/smbd" subj=system_u:system_r:smbd_t:s0 key=(null)ARCH=x86_64 SYSCALL=openat AUID="unset" UID="samba" GID="root" EUID="samba" SUID="root" FSUID="samba" EGID="samba" SGID="root" FSGID="samba"
type=PROCTITLE msg=audit(1583053020.069:110): proctitle=2F7573722F7362696E2F736D6264002D2D666F726567726F756E64002D2D6E6F2D70726F636573732D67726F7570
```

The key takeaway of this message is an *SELinux* context error where the source context is **smbd_t** and the destination context is **var_t**:
```
scontext=system_u:system_r:smbd_t:s0 tcontext=system_u:object_r:var_t:s0
```

In order to solve this, the following command should be run to set the correct *SELinux* context for the `/srv/samba` directory:
```sh
semanage fcontext -a -t samba_share_t "/srv/samba(/.*)?"
restorecon -Rv /srv/samba
# Relabeled /srv/samba from unconfined_u:object_r:var_t:s0 to unconfined_u:object_r:samba_share_t:s0
# Relabeled /srv/samba/test from unconfined_u:object_r:var_t:s0 to unconfined_u:object_r:samba_share_t:s0
```
