# 6. Partitioning a Hard Drive Using the *`MBR` Partitioning Scheme*
> ###### Exam Objectives Covered
> * 2.1
> * 3.1
> * 3.5
> * 4.1

> ###### Assumptions
> * Unpartitioned disk at `/dev/vdb`

---

## Disk Info
Confirm `/dev/vdb` has no partitions.
```sh
lsblk

#NAME          MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
# ...
# vdb           252:16   0   10G  0 disk

fdisk -l /dev/vdb
# Disk /dev/vdb: 10 GiB, 10737418240 bytes, 20971520 sectors
# Units: sectors of 1 * 512 = 512 bytes
# Sector size (logical/physical): 512 bytes / 512 bytes
# I/O size (minimum/optimal): 512 bytes / 512 bytes
```


## Partition the Disk
* Create *5GiB Primary Partition*
* Create *4GiB Primary Partition*
* Create *1GiB Extended Partition* For SWAP

```sh
fdisk /dev/vdb
# 5GiB Primary Partition
n # new partition
p # primary partition
1 # parition number
# hit return to accept starting sector
+5G # 5Gib partition

# 4GiB Primary Partition
n # new partition
p # primary partition
2 # parition number
# hit return to accept starting sector
+4G # 5Gib partition

# 1GiB Extended Partition
n # new partition
e # primary partition
3 # parition number
# Hit return to accept defaults
t # change partition type
3 # select extended partition
82 # Linux swap

w # write changes

partprobe
```

###### Verify
```sh
lsblk
# NAME          MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
# ...
# vdb           252:16   0   10G  0 disk
# ├─vdb1        252:17   0    5G  0 part
# ├─vdb2        252:18   0    4G  0 part
# └─vdb3        252:20   0 1023M  0 part
parted -l /dev/vdb
# Model: Virtio Block Device (virtblk)
# Disk /dev/vdb: 10.7GB
# Sector size (logical/physical): 512B/512B
# Partition Table: msdos
# Disk Flags:

# Number  Start   End     Size    Type     File system  Flags
#  1      1049kB  5370MB  5369MB  primary
#  2      5370MB  9665MB  4295MB  primary
#  3      9665MB  10.7GB  1073MB  primary
```


## Format/Create File Systems
```sh
mkfs.xfs -L mbr-xfs /dev/vdb1
# meta-data=/dev/vdb1              isize=512    agcount=4, agsize=327680 blks
#          =                       sectsz=512   attr=2, projid32bit=1
#          =                       crc=1        finobt=1, sparse=1, rmapbt=0
#          =                       reflink=1
# data     =                       bsize=4096   blocks=1310720, imaxpct=25
#          =                       sunit=0      swidth=0 blks
# naming   =version 2              bsize=4096   ascii-ci=0, ftype=1
# log      =internal log           bsize=4096   blocks=2560, version=2
#          =                       sectsz=512   sunit=0 blks, lazy-count=1
# realtime =none                   extsz=4096   blocks=0, rtextents=0

mkfs -t vfat -n MBR-VFAT /dev/vdb2
# mkfs.fat 4.1 (2017-01-24)

mkswap -L mbr-swap /dev/vdb3
# Setting up swapspace version 1, size = 1023 MiB (1072689152 bytes)
# LABEL=mbr-swap, UUID=c6c81a03-5089-4047-be3d-908a3add4c93
```

###### Verify
```sh
lsblk -o NAME,SIZE,TYPE,FSTYPE,LABEL /dev/vdb
# NAME           SIZE TYPE FSTYPE      LABEL
# vdb             10G disk
# ├─vdb1           5G part xfs         mbr-xfs
# ├─vdb2           4G part vfat        MBR-VFAT
# └─vdb3        1023M part swap        mbr-swap
```


## Mounting - Temporarily
```sh
mkdir -p /mount/mbr-{xfs,vfat}

blkid
# /dev/vdb1: LABEL="mbr-xfs" UUID="de356d91-9049-4184-bc44-782e61c92b7d" TYPE="xfs" PARTUUID="9ee33d56-01"
# /dev/vdb2: LABEL="MBR-VFAT" UUID="8821-E30E" TYPE="vfat" PARTUUID="9ee33d56-02"
# /dev/vdb3: LABEL="mbr-swap" UUID="c6c81a03-5089-4047-be3d-908a3add4c93" TYPE="swap" PARTUUID="9ee33d56-03"

mount UUID="de356d91-9049-4184-bc44-782e61c92b7d" /mount/mbr-xfs
mount LABEL="MBR-VFAT" /mount/mbr-vfat

# Pre-check off free memory
free -h
#               total        used        free      shared  buff/cache   available
# Mem:          1.8Gi       196Mi       1.5Gi       0.0Ki       103Mi       1.5Gi
# Swap:         1.0Gi        29Mi       994Mi

swapon UUID="c6c81a03-5089-4047-be3d-908a3add4c93"
```

###### Verify
```sh
free -h
#               total        used        free      shared  buff/cache   available
# Mem:          1.8Gi       197Mi       1.4Gi       1.0Mi       179Mi       1.5Gi
# Swap:         2.0Gi        29Mi       2.0Gi

df -h
# Filesystem             Size  Used Avail Use% Mounted on
# /dev/vdb1              5.0G   68M  5.0G   2% /mount/mbr-xfs
# /dev/vdb2              4.0G  4.0K  4.0G   1% /mount/mbr-vfat
```


## Mounting - Permanently
```sh
cat >> /etc/fstab << _EOF_
UUID="de356d91-9049-4184-bc44-782e61c92b7d" /mount/mbr-xfs     xfs     defaults    0 0
LABEL="MBR-VFAT"                            /mount/mbr-vfat    vfat    defaults    0 0
UUID="c6c81a03-5089-4047-be3d-908a3add4c93" swap               swap    defaults    0 0
_EOF_
```

###### Verify
```sh
mount -a
# should return no errors
reboot

# ... wait for server to reboot ...
df -h
# Filesystem             Size  Used Avail Use% Mounted on
# /dev/vdb1              5.0G   68M  5.0G   2% /mount/mbr-xfs
# /dev/vdb2              4.0G  4.0K  4.0G   1% /mount/mbr-vfat

free -h
#               total        used        free      shared  buff/cache   available
# Mem:          1.8Gi       218Mi       1.4Gi       9.0Mi       195Mi       1.4Gi
# Swap:         2.0Gi          0B       2.0Gi
```
