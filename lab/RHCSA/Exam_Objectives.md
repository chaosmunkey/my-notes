# EX200 Exam Objectives

## 1. Understand and use essential tools

1. Access a shell prompt and issue commands with correct syntax
2. Use input-output redirection (>, >>, |, 2>, etc.)
3. Use grep and regular expressions to analyze text
4. Access remote systems using SSH
5. Log in and switch users in multiuser targets
6. Archive, compress, unpack, and uncompress files using tar, star, gzip, and bzip2
7. Create and edit text files
8. Create, delete, copy, and move files and directories
9. Create hard and soft links
10. List, set, and change standard ugo/rwx permissions
11. Locate, read, and use system documentation including man, info, and files in /usr/share/doc

## 2. Operate running systems

1. Boot, reboot, and shut down a system normally
2. Boot systems into different targets manually
3. Interrupt the boot process in order to gain access to a system
4. Identify CPU/memory intensive processes and kill processes
5. Adjust process scheduling
6. Manage tuning profiles
7. Locate and interpret system log files and journals
8. Preserve system journals
9. Start, stop, and check the status of network services
10. Securely transfer files between systems

## 3. Configure local storage

1. List, create, delete partitions on MBR and GPT disks
2. Create and remove physical volumes
3. Assign physical volumes to volume groups
4. Create and delete logical volumes
5. Configure systems to mount file systems at boot by universally unique ID (UUID) or label
6. Add new partitions and logical volumes, and swap to a system non-destructively

## 4. Create and configure file systems

1. Create, mount, unmount, and use vfat, ext4, and xfs file systems
2. Mount and unmount network file systems using NFS
3. Extend existing logical volumes
4. Create and configure set-GID directories for collaboration
5. Configure disk compression
6. Manage layered storage
7. Diagnose and correct file permission problems

## 5. Deploy, configure, and maintain systems

1. Schedule tasks using at and cron
2. Start and stop services and configure services to start automatically at boot
3. Configure systems to boot into a specific target automatically
4. Configure time service clients
5. Install and update software packages from Red Hat Network, a remote repository, or from the local file system
6. Work with package module streams
7. Modify the system bootloader

## 6. Manage basic networking

1. Configure IPv4 and IPv6 addresses
2. Configure hostname resolution
3. Configure network services to start automatically at boot
4. Restrict network access using firewall-cmd/firewall

## 7. Manage users and groups

1. Create, delete, and modify local user accounts
2. Change passwords and adjust password aging for local user accounts
3. Create, delete, and modify local groups and group memberships
4. Configure superuser access

## 8. Manage security

1. Configure firewall settings using firewall-cmd/firewalld
2. Create and use file access control lists
3. Configure key-based authentication for SSH
4. Set enforcing and permissive modes for SELinux
5. List and identify SELinux file and process context
6. Restore default file contexts
7. Use boolean settings to modify system SELinux settings
8. Diagnose and address routine SELinux policy violations
