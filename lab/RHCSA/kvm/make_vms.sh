#!/bin/bash

###################################################
#                     NETWORK                     #
###################################################
NETWORKS=$(virsh net-list --name)

if [[ ! " ${NETWORKS[*]} " =~ "internal" ]];
then
    echo "Creating 'isolated' network"

    virsh net-create ./isolated-network.xml
fi

###################################################
#                      SERVER                     #
###################################################
SERVER=$(virsh list --all | grep "lab-server-01")

if [ -z "${SERVER}" ];
then
    echo "Creating 'lab-server-01'"

    virt-install --name "lab-server-01" \
    --cdrom ~/isos/CentOS-8.1.1911-x86_64-dvd1.iso \
    --disk size=30 \
    --disk size=4 \
    --memory 2048 \
    --vcpus=2

fi
