# Exam Objectives - RHEL 7

## 1. Understand and use essential tools

1. Access a shell prompt and issue commands with correct syntax
2. Use input-output redirection (>, >>, |, 2>, etc.)
3. Use grep and regular expressions to analyze text
4. Access remote systems using SSH
5. Log in and switch users in multi-user targets
6. Archive, compress, unpack, and uncompress files using tar, star, gzip, and bzip2
7. Create and edit text files

*Note: Red Hat may use applications during the exam that are not included in Red Hat Enterprise Linux for the purpose of evaluating candidate's abilities to meet this objective.*

## 2. Operate running systems

1. Boot, reboot, and shut down a system normally
2. Boot systems into different targets manually
3. Interrupt the boot process in order to gain access to a system
4. Identify CPU/memory intensive processes, adjust process priority with renice, and kill processes
5. Locate and interpret system log files and journals
6. Access a virtual machine's console
7. Start and stop virtual machines
8. Start, stop, and check the status of network services
9. Securely transfer files between systems

## 3. Configure local storage

1. List, create, delete partitions on MBR and GPT disks
2. Create and remove physical volumes, assign physical volumes to volume groups, and create and delete logical volumes
3. Configure systems to mount file systems at boot by universally unique ID (UUID) or label
4. Add new partitions and logical volumes, and swap to a system non-destructively

## 4. Create and configure file systems

1. Create, mount, unmount, and use vfat, ext4, and xfs file systems
2. Mount and unmount CIFS and NFS network file systems
3. Extend existing logical volumes
4. Create and configure set-GID directories for collaboration
5. Create and manage access control lists
6. Diagnose and correct file permission problems

## 5. Deploy, configure, and maintain systems

1. Configure networking and hostname resolution statically or dynamically
2. Schedule tasks using at and cron
3. Start and stop services and configure services to start automatically at boot
4. Configure systems to boot into a specific target automatically
5. Install Red Hat Enterprise Linux systems as virtual guests
6. Configure systems to launch virtual machines at boot
7. Configure network services to start automatically at boot
8. Configure a system to use time services
9. Install and update software packages from Red Hat Network, a remote repository, or from the local file system
10. Update the kernel package appropriately to ensure a bootable system
11. Modify the system bootloader

# 6. Manage users and groups

1. Create, delete, and modify local user accounts
2. Change passwords and adjust password aging for local user accounts
3. Create, delete, and modify local groups and group memberships
4. Configure a system to use an existing authentication service for user and group information

## 7. Manage security

1. Configure firewall settings using firewall-config, firewall-cmd, or iptables
2. Configure key-based authentication for SSH
3. Set enforcing and permissive modes for SELinux
4. List and identify SELinux file and process context
5. Restore default file contexts
6. Use boolean settings to modify system SELinux settings
7. Diagnose and address routine SELinux policy violations
