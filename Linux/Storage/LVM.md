# LVM - Logical Volume Manager

LVM provides a method for addressing a pool of space to manage storage.

A *logical volume* lives within a *volume group* which can span multiple *physical volumes*.


## *Physical Volume*

### Creating a *Physical Volume*
```sh
pvcreate /dev/dev1 /dev/dev2 /dev/dev3 ...
      Physical volume "/dev/dev1" successfully created.
      Physical volume "/dev/dev2" successfully created.
      Physical volume "/dev/dev3" successfully created.
      ...
```

### Displaying *Physical Volumes*
```sh
pvdisplay
      --- Physical volume ---
      PV Name               /dev/dev1
      VG Name               vg-name
      PV Size               1.00GiB / not usable 4.00 MiB
      Allocatable           yes 
      PE Size               4.00 MiB
      Total PE              255
      Free PE               255
      Allocated PE          0
      PV UUID               VJ7ypM-AicW-3n08-WtSw-ap05-l6Z4-KbYjcb

      --- Physical volume ---
      PV Name               /dev/sdb1
      VG Name               vg-name
      PV Size               1.00 GiB / not usable 4.00 MiB
      Allocatable           yes 
      PE Size               4.00 MiB
      Total PE              255
      Free PE               255
      Allocated PE          0
      PV UUID               u7cAre-LUrU-8V3h-dZPX-8KxU-qSDq-BpN2d1
      ...
```


## *Volume Group*

### Creating a *Volume Group*
```sh
vgcreate vg-name /dev/dev1 /dev/dev2 /dev/dev3 ...
      Volume group "vg-name" successfully created
```

### Displaying *Volume Groups*
```sh
vgdisplay 
      --- Volume group ---
      VG Name               vg-name
      System ID             
      Format                lvm2
      Metadata Areas        2
      Metadata Sequence No  1
      VG Access             read/write
      VG Status             resizable
      MAX LV                0
      Cur LV                0
      Open LV               0
      Max PV                0
      Cur PV                2
      Act PV                2
      VG Size               2.00 GiB
      PE Size               4.00 MiB
      Total PE              510
      Alloc PE / Size       0 / 0   
      Free  PE / Size       510 / 2.00 GiB
      VG UUID               z6eapT-i2Z2-EdmT-nZYI-YJfE-il2B-MducA4
```


## *Logical Volumes*

### Creating a *Logical Volume*
```sh
lvcreate -L 100m vg-name -n FirstLV
      Logical volume "FirstLV" created.
```

### Displaying *Logical Volumes*
```sh
lvdisplay
      --- Logical volume ---
      LV Path                /dev/vg-name/FirstLV
      LV Name                FirstLV
      VG Name                vg-name
      LV UUID                2Ye3q0-mJCs-XigH-l1Y4-DMGM-OuEa-NACUfc
      LV Write Access        read/write
      LV Creation host, time host.example.com, 2020-02-06 18:08:20 +0000
      LV Status              available
      # open                 0
      LV Size                100.00 MiB
      Current LE             25
      Segments               1
      Allocation             inherit
      Read ahead sectors     auto
      - currently set to     8192
      Block device           253:2
```
