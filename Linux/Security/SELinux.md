# `SELinux`

## Installing manuals
```sh
man -k _selinux
# pam_selinux (8)      - PAM module to set the default security context
```
Not much there... Will need to install the manuals.

```sh
yum whatprovides */sepolicy
# policycoreutils-devel-2.9-3.el8.i686 : SELinux policy core policy devel utilities
# Repo        : BaseOS
# Matched from:
# Filename    : /usr/bin/sepolicy
# Filename    : /usr/share/bash-completion/completions/sepolicy

# policycoreutils-devel-2.9-3.el8.x86_64 : SELinux policy core policy devel utilities
# Repo        : BaseOS
# Matched from:
# Filename    : /usr/bin/sepolicy
# Filename    : /usr/share/bash-completion/completions/sepolicy

# python3-policycoreutils-2.9-3.el8.noarch : SELinux policy core python3 interfaces
# Repo        : @System
# Matched from:
# Filename    : /usr/lib/python3.6/site-packages/sepolicy

# python3-policycoreutils-2.9-3.el8.noarch : SELinux policy core python3 interfaces
# Repo        : BaseOS
# Matched from:
# Filename    : /usr/lib/python3.6/site-packages/sepolicy
```
Provided by the **policycoreutils-devel** package:

```sh
yum install policycoreutils-devel


sepolicy --help
# usage: sepolicy [-h] [-P POLICY]
#                 {booleans,communicate,generate,gui,interface,manpage,network,transition}
#                 ...

# SELinux Policy Inspection Tool

# positional arguments:
# ...
#     manpage             Generate SELinux man pages
# ...
```

Run `man sepolicy` to see how to generate the *man pages*. This *man page* mentions another *man page*: **sepolicy-manpage(8)**. Look at this *man page*: `man 8 sepolicy-manpage`

### Generating the *man pages*
```sh
sepolicy manpage -a -p /usr/share/man/man8
# update list of man pages
mandb
```

