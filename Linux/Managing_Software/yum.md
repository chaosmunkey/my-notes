# YUM - *Yellowdog Updater, Modified*




### *yum* History

###### View History
*yum* can provide you with a history of actions performed with the tool.

```sh
yum history

      ID     | Command line             | Date and time    | Action(s)      | Altered
      -------------------------------------------------------------------------------
           3 | install -y setools-conso | 2020-02-07 19:26 | Install        |    1
           2 | install nmap             | 2020-02-06 20:43 | Install        |    1
           1 |                          | 2020-01-27 20:46 | Install        | 1335 EE
```

###### Undo Action

```sh
yum history undo 3

      Undoing transaction 3, from Fri 07 Feb 2020 19:26:32 GMT
          Install setools-console-4.2.0-2.el8.x86_64 @BaseOS
      ...
      Removed:
        setools-console-4.2.0-2.el8.x86_64
      Complete!
```
