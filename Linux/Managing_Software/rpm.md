# RPM - *Red Hat Package Manager*

`rpm` is a package manager originally designed to install software on Red Hat servers. It also acts as a way to manage said software after installation; upgrading, removing, etc.

One of the downsides to using `rpm` is it doesn't handle fetching and installing any dependencies a package might need to run.


## Installing *rpm* Packages

### The *old* Way

Simply run the command:

```sh
rpm -Uvh <package.rpm>
```

### The *newer/better* Way

Installing an RPM package using the `rpm` command means that package is added to the *RPM database*. This means that *YUM* has no knowledge of the package, plus it means that you could end up in *dependency hell* if you're missing some require packages.

To get around this, use the `yum`/`dnf` command to install the package. Not only will *YUM* install all of the dependencies for you, but it will add the package to both the *RPM* and *YUM* databases.

```sh
yum install <package.rpm>
```
