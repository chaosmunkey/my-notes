# `nmcli`


## Devices

#### GUI Manager
It's possible to edit the network connections using a GUI manager by simply typing `nm-connection-manager`.


### Command Line

#### Text Based Editor
When using the command line, it's possible to use a *text based editor* to edit the network interfaces. This can be achieved by running the command `nmtui`.

This tool has been known to cause `seg faults` when used, so it's best to be used with caution.


#### "Hardcore mode"

##### Device Status
```sh
nmcli device [status]

    DEVICE  TYPE      STATE         CONNECTION
    ens3    ethernet  connected     ens3
    ens8    ethernet  disconnected  --
    lo      loopback  unmanaged     --
```

##### Connections
###### Showing Connections on the System

```sh
nmcli c[onnection] s[how]

    NAME  UUID                                  TYPE      DEVICE
    ens3  22c68c56-2533-4a27-a706-6a6560494876  ethernet  ens3
```

###### Adding/Modifying Connections

```sh
# DHCP Connection
nmcli c[onnection] add con-name <name> autoconnect [yes|no] type ethernet ifname <device>

# Static - No Gateway
nmcli c a con-name "static-con" autoconnect yes type ethernet ifname ens8 ip4 <ip>/<netmask>

# Adding Gateway
nmcli c mod[ify] "static-con" gw4 <gateway-ip>

# Adding DNS Servers
nmcli c mod "static-con" ipv4.dns <dns-ip>

# Adding Additional DNS Servers
nmcli c mod "static-con" +ipv4.dns <dns-ip>
```

###### Up/Down Connection
```sh
nmcli c u[p] <con-name>
nmcli c do[wn] <con-name>
```
